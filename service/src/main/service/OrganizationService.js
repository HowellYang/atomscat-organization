let OrganizationDao = require('../dao/OrganizationDao');

class OrganizationService {
    getOrganizationList(call, callback) {
        let organizationDao = new OrganizationDao();
        organizationDao.getOrganizationList(function (list) {
            callback(list);
        });
    }

    getOrganizationPage(call, callback) {
        try {
            let organizationDao = new OrganizationDao();
            organizationDao.getOrganizationPage(call.request.commonPageRequest.pageNum,
                call.request.commonPageRequest.pageSize, call.request.organization, function (list) {
                    let totalPage = list[1][0][0].totalCount / call.request.commonPageRequest.pageSize;
                    if (list[1][0][0].totalCount % call.request.commonPageRequest.pageSize > 0) {
                        totalPage += 1;
                    }
                    let response = {
                        "commonPageResponse": {
                            "code": "000000",
                            "content": "成功",
                            "pageNum": call.request.commonPageRequest.pageNum,
                            "pageSize": call.request.commonPageRequest.pageSize,
                            "totalCount": list[1][0][0].totalCount,
                            "totalPage": totalPage
                        },
                        "organization": list[0][0]
                    };
                    callback(response);
                });
        } catch (err) {
            let response = {
                "commonPageResponse": {
                    "code": "800800",
                    "content": "失败"
                }
            };
            callback(response);
        }
    }

}

module.exports = OrganizationService;
