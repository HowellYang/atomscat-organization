const OrganizationController = require("../controller/OrganizationController");

/**
 * 统一初始化，服务注册
 */
class RPC {
    init() {
        new OrganizationController().init();
    }
}
module.exports = RPC;
