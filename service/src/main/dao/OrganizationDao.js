const mysql = require('../config/mysql');


class OrganizationDao {
    getOrganizationList(callback) {
        let sqlParamsEntity = [];
        let sql1 = "SELECT * FROM t_organization";
        let param1 = {};
        sqlParamsEntity.push(mysql.sqlParamEntity(sql1, param1));
        mysql.execTrans(sqlParamsEntity, function(err, info, result){
            if(err){
                console.error("事务执行失败");
            }else{
                console.info(result[0][0].length);
                callback(result[0][0]);
                console.log("done.");
            }
        });
    }

    getOrganizationPage(pageNum, pageSize, organization, callback) {
        let sqlParamsEntity = [];
        let sql1 = "SELECT * FROM t_organization";
        let param1 = organization;
        sqlParamsEntity.push(mysql.sqlParamPageEntity(sql1, param1, pageNum, pageSize));
        let sql2 = "SELECT * FROM t_organization";
        let param2 = organization;
        sqlParamsEntity.push(mysql.sqlParamCountEntity(sql2, param2));

        mysql.execTrans(sqlParamsEntity, function(err, info, result){
            if(err){
                console.error("事务执行失败");
            }else{
                callback(result);
                console.log("done.");
            }
        });
    }

}
module.exports = OrganizationDao;
