const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const OrganizationService = require('../service/OrganizationService');

class OrganizationController {
    /**
     * Implements RPC method.
     */
    getOrganizationList(call, callback) {
        let organizationService = new OrganizationService();
        organizationService.getOrganizationList(call, function (list) {
            let response = {
                "commonResponse": {
                    "code": "000000",
                    "content": "成功"
                },
                "organization": list
            };
            callback(null, response);
        });
    }

    getOrganizationPage(call, callback) {
        let organizationService = new OrganizationService();
        organizationService.getOrganizationPage(call, function (list) {
            callback(null, list);
        });
    }

    saveOrganization(call, callback) {
        callback(null, {});
    }

    /**
     * 服务注册
     */
    init() {
        const PROTO_PATH = __dirname + '/../../../../api/src/protos/OrganizationService.proto';
        let packageDefinition = protoLoader.loadSync(
            PROTO_PATH,
            {keepCase: true,
                longs: String,
                enums: String,
                defaults: true,
                oneofs: true
            });
        let atomscat = grpc.loadPackageDefinition(packageDefinition).atomscat;

        server.addService(atomscat.OrganizationService.service, {
            getOrganizationList: this.getOrganizationList,
            getOrganizationPage: this.getOrganizationPage,
            saveOrganization: this.saveOrganization
            });
    }
}

module.exports = OrganizationController;
