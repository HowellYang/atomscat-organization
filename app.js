let path = require('path');
let RPC = require("./service/src/main/config/rpc");

let env = process.env.NODE_ENV || 'prod';
// 载入配置文件
let file = path.resolve("./service/src/resources", env);
try {
    global.config = require(file);
    console.log('Load config: [%s] %s', env, file);
} catch (err) {
    console.error('Cannot load config: [%s] %s', env, file);
    throw err;
}

let grpc = require('grpc');
let server = new grpc.Server();
global.server = server;

new RPC().init();


server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
server.start();

